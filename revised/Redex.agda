--  ******************************************* --
--            Redex and contraction             --
--  ******************************************* --
module Redex where

open import Data.Empty
open import Relation.Nullary

open import Relation.Binary.PropositionalEquality

open import Terms  

data Redex : Ty → Set where
  var_//_ : ∀ {Γ σ} →
    (x : Var Γ σ) → (ρ : Env Γ) → Redex σ
  _·_//_  : ∀ {Γ σ τ} →
    (t₁ : Tm Γ (σ ⇒ τ)) → (t₂ : Tm Γ σ) → (ρ : Env Γ) → Redex τ
  ƛ_//_·_ : ∀ {Γ σ τ} →
    (t : Tm (Γ , σ) τ) → (ρ : Env Γ) → (c : Closed σ) → Redex τ

-- Reduction step

contract : ∀ {σ} → Redex σ → Closed σ

contract (var x // ρ)  = ρ ! x
contract (t₁ · t₂ // ρ ) = ⟪ ⟪ t₁ / ρ ⟫ · ⟪ t₂ / ρ ⟫ ⟫
contract (ƛ t // ρ · c) = ⟪ t / (ρ , c) ⟫

⌈_⌉ : ∀ {σ} → Redex σ → Closed σ

⌈ var x // ρ ⌉ = ⟪ var x / ρ ⟫
⌈ t₁ · t₂ // ρ ⌉ = ⟪ t₁ · t₂ / ρ ⟫
⌈ ƛ t // ρ · c ⌉ = ⟪ ⟪ ƛ t / ρ ⟫ · c ⟫

data EvalCon : Ty → Ty → Set where
  ε   : ∀ {σ} → EvalCon σ σ
  _,_ : ∀ {σ τ τ′} → (c : Closed σ) → (ctx : EvalCon τ τ′) →
          EvalCon (σ ⇒ τ) τ′

_↪_ : ∀ {σ τ} → Closed σ → EvalCon σ τ → Closed τ

f ↪ ε = f
f ↪ (c , ctx) = ⟪ f · c ⟫ ↪ ctx


data Decomposition : ∀ {σ} → Closed σ → Set where
  ⟪ƛ_/_⟫     : ∀ {σ τ Γ} → (body : Tm (Γ , σ) τ) → (ρ : Env Γ) → 
    Decomposition ⟪ ƛ body / ρ ⟫
  _↩_ : ∀ {σ τ} → (r : Redex σ) → (ctx : EvalCon σ τ) → 
    Decomposition (⌈ r ⌉ ↪ ctx)


load : ∀ {σ τ} → (c : Closed σ) (ctx : EvalCon σ τ) →
         Decomposition (c ↪ ctx)

load ⟪ var x / ρ ⟫ ctx =
  (var x // ρ) ↩ ctx
load ⟪ ƛ t / ρ ⟫ ε =
  ⟪ƛ t / ρ ⟫
load ⟪ ƛ t / ρ ⟫ (c , ctx) =
  (ƛ t // ρ · c) ↩ ctx
load ⟪ t₁ · t₂ / ρ ⟫ ctx =
  (t₁ · t₂ // ρ) ↩ ctx
load ⟪ c₁ · c₂ ⟫ ctx =
  load c₁ (c₂ , ctx)


decompose : ∀ {σ} → (c : Closed σ) → Decomposition c
decompose c = load c ε


headReduce : ∀ {σ} → Closed σ → Closed σ
headReduce c with decompose c 
headReduce .(⟪ ƛ body / ρ ⟫) | ⟪ƛ body / ρ ⟫ =
  ⟪ ƛ body / ρ ⟫
headReduce .(⌈ r ⌉ ↪ ctx) | r ↩ ctx =
  contract r ↪ ctx


--  ******************************************* --
--          Lemmas about decomposition          --
--  ******************************************* --

decompose∘↪ : ∀ {σ τ} → (c : Closed σ) (ctx : EvalCon σ τ) →
  decompose (c ↪ ctx) ≡ load c ctx

decompose∘↪ c ε = refl
decompose∘↪ c (c′ , ctx) = decompose∘↪ ⟪ c · c′ ⟫ ctx

load∘⌈⌉ : ∀ {σ τ} → (r : Redex σ) (ctx : EvalCon σ τ) →
  load ⌈ r ⌉ ctx ≡ r ↩ ctx

load∘⌈⌉ (var x // ρ) ctx = refl
load∘⌈⌉ (t₁ · t₂ // ρ) ctx = refl
load∘⌈⌉ (ƛ t // ρ · c) ctx = refl

headReduce∘↪ : ∀ {σ τ} → (r : Redex σ) (ctx : EvalCon σ τ) →
  headReduce (⌈ r ⌉ ↪ ctx) ≡ (contract r) ↪  ctx

headReduce∘↪ r ctx
  rewrite decompose∘↪ ⌈ r ⌉ ctx
        | load∘⌈⌉ r ctx
  = refl


--  ******************************************* --
--                Reversed EvalCon View         --
--  ******************************************* --

_●_ : ∀ {σ τ₁ τ₂} → EvalCon σ (τ₁ ⇒ τ₂) → Closed τ₁ → EvalCon σ τ₂

ε ● c = c , ε
(c′ , ctx) ● c = c′ , (ctx ● c)

data ●-View : {σ τ : Ty} → EvalCon σ τ → Set where
  ε   : ∀ {σ} → ●-View {σ} {σ} ε
  _,ʳ_ : ∀ {σ τ τ′} → (ctx : EvalCon σ (τ ⇒ τ′)) (c : Closed τ) →
           ●-View (ctx ● c)

●-view : ∀ {σ τ} → (ctx : EvalCon σ τ) → ●-View ctx

●-view ε = ε
●-view (c , ctx)
  with ●-view ctx
●-view (c , .ε)
  | ε = ε ,ʳ c
●-view (c , .(ctx ● c′))
  | ctx ,ʳ c′ = (c , ctx) ,ʳ c′

--  The ↪∘● function is the main property of ● that we use

↪∘● : ∀ {σ v w} →
   (c : Closed v) (ctx : EvalCon σ (v ⇒ w)) → 
   (t : Closed σ) →
   t ↪ (ctx ● c) ≡ ⟪ t ↪ ctx · c ⟫

↪∘● c ε t =
  refl
↪∘● c (c′ , ctx) t = begin
  t ↪ ((c′ , ctx) ● c)
    ≡⟨ refl ⟩
  ⟪ t · c′ ⟫ ↪ (ctx ● c)
    ≡⟨ ↪∘● c ctx ⟪ t · c′ ⟫ ⟩
  ⟪ ⟪ t · c′ ⟫ ↪ ctx · c ⟫
    ≡⟨ refl ⟩
  ⟪ t ↪ (c′ , ctx) · c ⟫
  ∎
  where open ≡-Reasoning

--  Some useful auxiliary properties of equalities

⟪·⟫-inv₁ : ∀ {σ τ} →
  {f₁ f₂ : Closed (σ ⇒ τ)} {c₁ c₂ : Closed σ} →
  ⟪ f₁ · c₁ ⟫ ≡ ⟪ f₂ · c₂ ⟫ → f₁ ≡ f₂

⟪·⟫-inv₁ refl = refl

⟪·⟫-inv₂ : ∀ {σ τ} →
  {f₁ f₂ : Closed (σ ⇒ τ)} {c₁ c₂ : Closed σ} →
  ⟪ f₁ · c₁ ⟫ ≡ ⟪ f₂ · c₂ ⟫ → c₁ ≡ c₂

⟪·⟫-inv₂ refl = refl

⟪·⟫-inv-σ : ∀ {σ₁ σ₂ τ} →
  {f₁ : Closed (σ₁ ⇒ τ)} {f₂ : Closed (σ₂ ⇒ τ)}
  {c₁ : Closed σ₁} {c₂ : Closed σ₂} →
  ⟪ f₁ · c₁ ⟫ ≡ ⟪ f₂ · c₂ ⟫ → σ₁ ≡ σ₂
⟪·⟫-inv-σ refl = refl

--  ******************************************* --
--     The property needed in lemma1            --
--  ******************************************* --

headReduce∘⟪·⟫′ : ∀ {σ τ} →
  (f : Closed (σ ⇒ τ)) (c : Closed σ) →
  {fc : Closed τ} (eq : ⟪ f · c ⟫ ≡ fc) →
  ¬ Value f →
  headReduce fc ≡ ⟪ headReduce f · c ⟫

headReduce∘⟪·⟫′ f c {fc} eq nvf with decompose fc

headReduce∘⟪·⟫′ f c () nvf | ⟪ƛ body / ρ ⟫

headReduce∘⟪·⟫′ f c eq nvf | r ↩ ctx
  with ●-view ctx
headReduce∘⟪·⟫′ f c () nvf
  | (var x // ρ) ↩ .ε | ε
headReduce∘⟪·⟫′ f c () nvf
  | (t₁ · t₂ // ρ) ↩ .ε | ε
headReduce∘⟪·⟫′ f c eq nvf
  | (ƛ t // ρ · c′) ↩ .ε | ε
  with ⟪·⟫-inv-σ eq
... | refl rewrite ⟪·⟫-inv₁ eq =
  ⊥-elim (nvf ⟪ ƛ t / ρ ⟫)
headReduce∘⟪·⟫′ f c eq nvf
  | r ↩ .(ctx ● c′) | ctx ,ʳ c′
  rewrite ↪∘● c′ ctx (contract r)
        | ↪∘● c′ ctx (⌈_⌉ r) with ⟪·⟫-inv-σ eq
... | refl rewrite ⟪·⟫-inv₂ eq | ⟪·⟫-inv₁ eq | headReduce∘↪ r ctx
  = refl

headReduce∘⟪·⟫ : ∀ {σ τ} →
  (f : Closed (σ ⇒ τ)) (c : Closed σ) →
  ¬ Value f →
  headReduce ⟪ f · c ⟫ ≡ ⟪ headReduce f · c ⟫

headReduce∘⟪·⟫ f c nvf = headReduce∘⟪·⟫′ f c refl nvf


headReduce∘⟪⟪·⟫·⟫ : ∀ {α β γ} →
  (c₁ : Closed (α ⇒ (β ⇒ γ))) (c₂ : Closed α) (c₃ : Closed β) →
  headReduce ⟪ ⟪ c₁ · c₂ ⟫ · c₃ ⟫ ≡ ⟪ headReduce ⟪ c₁ · c₂ ⟫  · c₃ ⟫

headReduce∘⟪⟪·⟫·⟫ c₁ c₂ c₃ = headReduce∘⟪·⟫ ⟪ c₁ · c₂ ⟫ c₃ (λ ())