--  ******************************************* --
--            The Krivine Machine               --
--  ******************************************* --

module Section6 where

open import Data.Product
open import Data.Unit
open import Data.Empty

open import Function

open import Relation.Binary.PropositionalEquality

open import Terms
open import Redex
import Section5

mutual

  ValidClosure : ∀ {σ} → (c : Closed σ) → Set
  ValidClosure ⟪ t / ρ ⟫ = ValidEnv ρ
  ValidClosure ⟪ c₁ · c₂ ⟫ = ⊥

  ValidEnv : ∀ {Γ} → (ρ : Env Γ) → Set
  ValidEnv ε = ⊤
  ValidEnv (ρ , c) = ValidEnv ρ × ValidClosure c

ValidEvalCon : ∀ {σ τ} → (ctx : EvalCon σ τ) → Set

ValidEvalCon ε = ⊤
ValidEvalCon (⟪ t / ρ ⟫ , ctx) = ValidEnv ρ × ValidEvalCon ctx
ValidEvalCon (⟪ c₁ · c₂ ⟫ , ctx) = ⊥


getCon : ∀ {σ} → Σ (Closed σ) ValidClosure → Con
getCon (⟪_/_⟫ {Γ} t ρ , vc) = Γ
getCon (⟪ c₁ · c₂ ⟫ , ())


getEnv : ∀ {σ} → (exc : Σ (Closed σ) ValidClosure) → Env (getCon exc)

getEnv (⟪ t / ρ ⟫ , vc) = ρ
getEnv (⟪ c₁ · c₂ ⟫ , ())

getTm : ∀ {σ} → (exc : Σ (Closed σ) ValidClosure) → Tm (getCon exc) σ

getTm (⟪ t / ρ ⟫ , vc) = t
getTm (⟪ c₁ · c₂ ⟫ , ())


getClosure : ∀ {σ Γ} → (x : Var Γ σ) → (ρ : Env Γ) → (vρ : ValidEnv ρ) → 
               Σ (Closed σ) ValidClosure

getClosure vz (ρ , ⟪ t / ρ' ⟫) (vρ , vc) = ⟪ t / ρ' ⟫ , vc
getClosure vz (ρ , ⟪ c₁ · c₂ ⟫) (vρ , ())
getClosure (vs x) (ρ , c) (vρ , vc) = getClosure x ρ vρ


lookupClosure : ∀ {σ Γ} → (ρ : Env Γ) (x : Var Γ σ) →
                  (vρ : ValidEnv ρ) →  
                  let exc = getClosure x ρ vρ in
                  (ρ ! x) ≡ ⟪ getTm exc / getEnv exc ⟫

lookupClosure ε () tt
lookupClosure (ρ , ⟪ t / ρ′ ⟫) vz (vρ , vc) = refl
lookupClosure (ρ , ⟪ c₁ · c₂ ⟫) vz (vρ , ())
lookupClosure (ρ , c) (vs x) (vρ , vc) = lookupClosure ρ x vρ


lookupLemma : ∀ {σ Γ} → (ρ : Env Γ) (x : Var Γ σ) (vρ : ValidEnv ρ) →
                ValidEnv (getEnv (getClosure x ρ vρ))

lookupLemma ε () tt
lookupLemma (ρ , ⟪ t / ρ' ⟫) vz (vρ , vc) = vc
lookupLemma (ρ , ⟪ c₁ · c₂ ⟫) vz (vρ , ())
lookupLemma (ρ , c) (vs x) (vρ , vc) = lookupLemma ρ x vρ


data Trace : ∀ {Γ σ τ} → Tm Γ σ → Env Γ → EvalCon σ τ → Set where
  done :
    ∀ {Γ σ τ} → {ρ : Env Γ} (body : Tm (Γ , τ) σ) →
      Trace (ƛ body) ρ ε
  closure :
    ∀ {σ τ Γ} → {ctx : EvalCon σ τ} {ρ : Env Γ} (x : Var Γ σ)
      (vρ : ValidEnv ρ) → 
      let exc = getClosure x ρ vρ in
      (θ : Trace (getTm exc) (getEnv exc) ctx) →
      Trace (var x) ρ ctx
  app :
    ∀ {Γ σ τ₁ τ₂} → {ρ : Env Γ} {ctx : EvalCon τ₁ τ₂}
      (t₁ : Tm Γ (σ ⇒ τ₁)) (t₂ : Tm Γ σ) →
      (θ : Trace t₁ ρ (⟪ t₂ / ρ ⟫ , ctx)) →
      Trace (t₁ · t₂) ρ ctx
  beta :
    ∀ {Γ σ σ′ τ Γ′} → {ρ : Env Γ} (ctx : EvalCon σ τ) →
      (t′ : Tm Γ′ σ′) → (ρ′ : Env Γ′) →
      (body : Tm (Γ , σ′) σ) → 
      (θ : Trace body (ρ , ⟪ t′ / ρ′ ⟫) ctx) →
      Trace (ƛ body) ρ (⟪ t′ / ρ′ ⟫ , ctx)


refocus : ∀ {Γ σ τ} → {t : Tm Γ σ} {ctx : EvalCon σ τ} (ρ : Env Γ) →
            (θ : Trace t ρ ctx) → (∃ λ (c : Closed τ) → Value c)

refocus ρ (done body) =
  ⟪ ƛ body / ρ ⟫ , ⟪ ƛ body / ρ ⟫

refocus ρ (closure x vρ θ) =
  refocus (getEnv (getClosure x ρ vρ)) θ

refocus ρ (app t₁ t₂ θ) =
  refocus ρ θ

refocus ρ (beta ctx t′ ρ′ body θ) =
  refocus (ρ , ⟪ t′ / ρ′ ⟫) θ


termination : ∀ {σ} → (t : Tm ε σ) → Trace t ε ε

termination t =
  helper t ε ε tt tt (Section5.termination ⟪ t / ε ⟫)
  where
  helper : ∀ {Γ σ τ} → (t : Tm Γ σ) (ctx : EvalCon σ τ) (ρ : Env Γ) →
           (vctx : ValidEvalCon ctx) → (vρ : ValidEnv ρ) →
           Section5.Trace (Section5.refocus ⟪ t / ρ ⟫ ctx) →
           Trace t ρ ctx
  helper (var x) ctx ρ vctx vρ (Section5.step θ)
    rewrite lookupClosure ρ x vρ =
    let vc = getClosure x ρ vρ in
    closure x vρ
            (helper (getTm vc) ctx (getEnv vc) vctx (lookupLemma ρ x vρ) θ)
  helper (ƛ t) ε ρ vctx vρ θ = done t
  helper (ƛ t) (⟪ t′ / ρ′ ⟫ , ctx) ρ (vρ′ , vctx) vρ (Section5.step θ) =
    beta ctx t′ ρ′ t (helper t ctx (ρ , ⟪ t′ / ρ′ ⟫) vctx (vρ , vρ′) θ)
  helper (ƛ t) (⟪ c₁ · c₂ ⟫ , ctx) ρ () vρ (Section5.step θ)
  helper (t₁ · t₂) ctx ρ vctx vρ (Section5.step θ) =
    app t₁ t₂ (helper t₁ (⟪ t₂ / ρ ⟫ , ctx) ρ (vρ , vctx) vρ θ)


evaluate : ∀ {σ} → (t : Tm ε σ) → (∃ λ (c : Closed σ) → Value c)
evaluate t = refocus ε (termination t)


correctness :
  ∀ {Γ σ v} →
    (t : Tm Γ σ) (ctx : EvalCon σ v) (ρ : Env Γ) →
    (θ : Trace t ρ ctx) → 
    (θ′ : Section5.Trace (Section5.refocus ⟪ t / ρ ⟫ ctx)) →
    refocus ρ θ ≡
      Section5.iterate (Section5.refocus ⟪ t / ρ ⟫ ctx) θ′

correctness (var x) ctx ρ (closure .x vρ θ) (Section5.step θ')
  rewrite lookupClosure ρ x vρ =
  let vc = getClosure x ρ vρ in
  correctness (getTm vc) ctx (getEnv vc) θ θ'
correctness (ƛ t) .ε ρ (done .t) Section5.done =
  refl
correctness (ƛ t) .(⟪ t′ / ρ′ ⟫ , ctx) ρ
  (beta ctx t′ ρ′ .t θ) (Section5.step θ') =
  correctness t ctx (ρ , ⟪ t′ / ρ′ ⟫) θ θ'
correctness (t₁ · t₂) ctx ρ (app .t₁ .t₂ θ) (Section5.step θ') =
  correctness t₁ (⟪ t₂ / ρ ⟫ , ctx) ρ θ θ'


corollary : ∀ {σ} → (t : Tm ε σ) →
              evaluate t ≡ Section5.evaluate ⟪ t / ε ⟫
corollary t =
  let θ = termination t in
  let θ′ = Section5.termination ⟪ t / ε ⟫ in
  correctness t ε ε θ θ′
