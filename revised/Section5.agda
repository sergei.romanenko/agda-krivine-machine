--  ******************************************* --
--                    Refocusing                --
--  ******************************************* --

module Section5 where

open import Data.Product

open import Function

open import Relation.Binary.PropositionalEquality

open ≡-Reasoning

open import Terms
open import Redex
import Section4


refocus : ∀ {σ τ} → (c : Closed σ) (ctx : EvalCon σ τ) →
            Decomposition (c ↪ ctx)

refocus ⟪ var x / ρ ⟫ ctx =
  (var x // ρ) ↩ ctx
refocus ⟪ ƛ t / ρ ⟫ ε =
  ⟪ƛ t / ρ ⟫
refocus ⟪ ƛ t / ρ ⟫ (c , ctx) =
  (ƛ t // ρ · c) ↩ ctx
refocus ⟪ t₁ · t₂ / ρ ⟫ ctx =
  (t₁ · t₂ // ρ) ↩ ctx
refocus ⟪ c₁ · c₂ ⟫ ctx =
  refocus c₁ (c₂ , ctx)


refocusCorrect : ∀ {σ τ} (c : Closed σ) (ctx : EvalCon σ τ) → 
  refocus c ctx ≡ decompose (c ↪ ctx)

refocusCorrect ⟪ var x / ρ ⟫ ε =
  refl
refocusCorrect ⟪ var x / ρ ⟫ (c , ctx)
  rewrite decompose∘↪ ⟪ ⟪ var x / ρ ⟫ · c ⟫ ctx = refl

refocusCorrect ⟪ ƛ t / ρ ⟫ ε =
  refl
refocusCorrect ⟪ ƛ t / ρ ⟫ (c , ctx)
  rewrite decompose∘↪ ⟪ ⟪ ƛ t / ρ ⟫ · c ⟫ ctx = refl

refocusCorrect ⟪ t₁ · t₂ / ρ ⟫ ε =
  refl
refocusCorrect ⟪ t₁ · t₂ / ρ ⟫ (c , ctx)
  rewrite decompose∘↪ ⟪ ⟪ t₁ · t₂ / ρ ⟫ · c ⟫ ctx = refl

refocusCorrect ⟪ c₁ · c₂ ⟫ ctx = refocusCorrect c₁ (c₂ , ctx)


data Trace : {σ : Ty} {c : Closed σ} → Decomposition c → Set where
   done : ∀ {Γ σ τ} → {body : Tm (Γ , σ) τ} → {ρ : Env Γ} →
            Trace ⟪ƛ body / ρ ⟫
   step : ∀ {σ τ} → {r : Redex σ} {ctx : EvalCon σ τ} →
            (θ : Trace (refocus (contract r) ctx)) →
            Trace  (r ↩ ctx)


termination : ∀ {σ} → (c : Closed σ) → Trace (refocus c ε)

termination c rewrite refocusCorrect c ε =
  helper (Section4.termination c)
  where
  helper : ∀ {σ} → {c : Closed σ} → Section4.Trace (decompose c) →
             Trace (decompose c)
  helper {c = c} θ with decompose c
  helper θ | ⟪ƛ body / ρ ⟫ = done
  helper (Section4.step θ) | r ↩ ctx =
    step (subst Trace (sym $ refocusCorrect (contract r) ctx) (helper θ))


iterate : ∀ {σ} → {c : Closed σ} → (d : Decomposition c) → Trace d →
            (∃ λ (c′ : Closed σ) → Value c′)

iterate ⟪ƛ body / ρ ⟫ θ =
  ⟪ ƛ body / ρ ⟫ , ⟪ ƛ body / ρ ⟫
iterate (r ↩ ctx) (step θ) =
  iterate (refocus (contract r) ctx) θ


evaluate : ∀ {σ} → Closed σ → (∃ λ (c′ : Closed σ) → Value c′)

evaluate c = iterate (refocus c ε) (termination c)


correctness : ∀ {σ} → {c : Closed σ}→ 
  (θ : Trace (refocus c ε)) → (θ′ : Section4.Trace (decompose c)) →
  iterate (refocus c ε) θ ≡ Section4.iterate (decompose c) θ′

correctness {c = c} θ θ′ rewrite refocusCorrect c ε =
  helper {c = c} θ θ′
  where
  helper : ∀ {σ} → {c : Closed σ} →
             (θ : Trace (decompose c)) → (θ′ : Section4.Trace (decompose c)) →
             iterate (decompose c) θ ≡ Section4.iterate (decompose c) θ′
  helper {c = c} θ θ′ with decompose c
  helper done Section4.done | ⟪ƛ body / ρ ⟫ = refl
  helper (step θ) (Section4.step θ′) | r ↩ ctx
    rewrite refocusCorrect (contract r) ctx
    = helper θ θ′

corollary : ∀ {σ} → (c : Closed σ) → evaluate c ≡ Section4.evaluate c

corollary c = correctness (termination c) (Section4.termination c)
