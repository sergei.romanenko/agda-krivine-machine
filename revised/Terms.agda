--  ******************************************* --
--                 Basic Terms                  --
--  ******************************************* --
module Terms where

--open import Data.List
open import Data.Empty
--open import Data.Unit
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality

-- Simple types

infixr 8 _⇒_

data Ty : Set where
  ○ : Ty
  _⇒_ : (σ τ : Ty) → Ty

-- De Bruijn contexts

data Con : Set where
  ε : Con
  _,_ : (Γ : Con) → (σ : Ty) → Con

-- De Bruijn indices (the set of variables)

data Var : Con → Ty → Set where
  vz : ∀ {Γ σ} → Var (Γ , σ) σ
  vs : ∀ {Γ σ τ} → (x : Var Γ σ) → Var (Γ , τ) σ

-- Removing a variable from a context

_-_ : {σ : Ty} → (Γ : Con) → Var Γ σ → Con
ε       - ()
(Γ , σ) - vz     = Γ
(Γ , τ) - (vs x) = (Γ - x) , τ

-- The set of terms

infixl 9 _·_

data Tm : Con → Ty → Set where
  var : ∀ {Γ σ} → (x : Var Γ σ) → Tm Γ σ
  ƛ   : ∀ {Γ σ τ} → (t : Tm (Γ , σ) τ) → Tm Γ (σ ⇒ τ)
  _·_ : ∀ {Γ σ τ} → (t₁ : Tm Γ (σ ⇒ τ)) → (t₂ : Tm Γ σ) → Tm Γ τ

-- Closed terms

mutual

  data Closed : Ty → Set where
    ⟪_/_⟫ : ∀ {Γ σ} → (t : Tm Γ σ) → (ρ : Env Γ) → Closed σ
    ⟪_·_⟫  : ∀ {σ τ} → (c₁ : Closed (σ ⇒ τ)) → (c₂ : Closed σ) → Closed τ

  data Env : Con → Set where
    ε   : Env ε
    _,_ : ∀ {Γ σ} → (ρ : Env Γ) → (c : Closed σ) → Env (Γ , σ)


infixl 4 _!_

_!_ : ∀ {Γ σ} → Env Γ → Var Γ σ → Closed σ

ε ! ()
ρ , c ! vz = c
ρ , c ! vs x = ρ ! x

-- Values

data Value {σ : Ty} : (c : Closed σ) → Set where
  ⟪_/_⟫ : ∀ {Γ : Con} (t : Tm Γ σ) (ρ : Env Γ) → Value ⟪ t / ρ ⟫

value? :  {σ : Ty} → (c : Closed σ) → Dec (Value c)

value? ⟪ t / ρ ⟫ = yes ⟪ t / ρ ⟫
value? ⟪ c₁ · c₂ ⟫ = no (λ ())

-- Contradictions

⟪/⟫≢⟪·⟫ :
  ∀ {Γ σ τ} → {t : Tm (Γ , σ) τ} →
  ∀ {t : Tm Γ τ} {ρ} {c₁ : Closed (σ ⇒ τ) } {c₂ : Closed σ} →
  ⟪ t / ρ ⟫ ≢ ⟪ c₁ · c₂ ⟫

⟪/⟫≢⟪·⟫ ()

¬Value⟪·⟫ : ∀ {σ τ} → {c₁ : Closed (σ ⇒ τ) } {c₂ : Closed σ} →
  ¬ Value ⟪ c₁ · c₂ ⟫

¬Value⟪·⟫ = λ ()