--  ******************************************* --
--           Iterated head reduction            --
--  ******************************************* --

module Section4 where

open import Data.Product
open import Data.Unit

open import Function

open import Relation.Binary.PropositionalEquality as P

open import Terms
open import Redex


data Trace : ∀ {σ} → {c : Closed σ} → Decomposition c → Set where
  done : ∀ {σ τ Γ} → {body : Tm (Γ , σ) τ} → {ρ : Env Γ} →
           Trace ⟪ƛ body / ρ ⟫
  step : ∀ {σ τ : Ty} {r : Redex σ} {ctx : EvalCon σ τ} →
           (θ : Trace (decompose (contract r ↪ ctx))) →
           Trace (r ↩ ctx)


iterate : ∀ {σ : Ty} → {c : Closed σ} → (d : Decomposition c) →
            (θ : Trace d) →
            (∃ λ (c′ : Closed σ) → Value c′)

iterate ⟪ƛ body / ρ ⟫ done =
  ⟪ ƛ body / ρ ⟫ , ⟪ ƛ body / ρ ⟫
iterate (r ↩ ctx) (step θ) =
  iterate (decompose (contract r ↪ ctx)) θ


Reducible : {σ : Ty} → (c : Closed σ) → Set

Reducible {○} c =
  Trace (decompose c)
Reducible {σ ⇒ τ} c =
  Trace (decompose c) ×
    ((c′ : Closed σ) → Reducible c′ → Reducible ⟪ c · c′ ⟫)


ReducibleEnv : ∀ {Γ} → (ρ : Env Γ) → Set
ReducibleEnv ε = ⊤
ReducibleEnv (ρ , c) = (Reducible c) × (ReducibleEnv ρ)


trace∘decompose : ∀ {σ} {c : Closed σ} →
  (θ : Trace (decompose (headReduce c))) → Trace (decompose c)

trace∘decompose {c = c} θ with decompose c
trace∘decompose θ | ⟪ƛ body / ρ ⟫ = θ
trace∘decompose θ | r ↩ ctx = step {r = r} {ctx = ctx} θ


lemma1 : ∀ {σ} → (c : Closed σ) → Reducible (headReduce c) → Reducible c

lemma1 {○} c h =
  trace∘decompose h
lemma1 {σ ⇒ τ} ⟪ var x / ρ ⟫ (θ , h) =
  trace∘decompose θ , (λ c′ rc′ → lemma1 ⟪ ⟪ var x / ρ ⟫ · c′ ⟫ (h c′ rc′))
lemma1 {σ ⇒ τ} ⟪ ƛ t / ρ ⟫  (θ , h) =
  θ , h
lemma1 {σ ⇒ τ} ⟪ t₁ · t₂ / ρ ⟫  (θ , h) =
  (trace∘decompose θ) , (λ c′ rc′ → lemma1 ⟪ ⟪ t₁ · t₂ / ρ ⟫ · c′ ⟫ (h c′ rc′))
lemma1 {σ ⇒ τ} ⟪ c₁ · c₂ ⟫  (θ , h) =
  trace∘decompose θ ,
    (λ c′ rc′ → lemma1 ⟪ ⟪ c₁ · c₂ ⟫ · c′ ⟫
      (subst Reducible (sym $ headReduce∘⟪⟪·⟫·⟫ c₁ c₂ c′) (h c′ rc′)))


reducible∘! : ∀ {Γ σ} (ρ : Env Γ) (x : Var Γ σ) →
              ReducibleEnv ρ → Reducible (ρ ! x)
reducible∘! ε () h
reducible∘! (ρ , c) vz (hc , hρ) = hc
reducible∘! (ρ , c) (vs x) (hc , hρ) = reducible∘! ρ x hρ


lemma2 : ∀ {Γ σ} → (t : Tm Γ σ) (ρ : Env Γ) → 
  ReducibleEnv ρ → Reducible ⟪ t / ρ ⟫

lemma2 (var x) ρ h =
  lemma1 ⟪ var x / ρ ⟫ (reducible∘! ρ x h)
lemma2 (ƛ t) ρ h =
  done ,
    (λ c′ rc′ → lemma1 ⟪ ⟪ ƛ t / ρ ⟫ · c′ ⟫ (lemma2 t (ρ , c′) (rc′ , h)))
lemma2 (t₁ · t₂) ρ h with lemma2 t₁ ρ h | lemma2 t₂ ρ h
... | θ , rc′ | r₂ =
  lemma1 ⟪ t₁ · t₂ / ρ ⟫ (rc′ ⟪ t₂ / ρ ⟫ r₂)


mutual    

  theorem : ∀ {σ} → (c : Closed σ) → Reducible c

  theorem ⟪ t / ρ ⟫ =
    lemma2 t ρ (envTheorem ρ)
  theorem ⟪ c₁ · c₂ ⟫ with theorem c₁ | theorem c₂
  ... | θ , rc′ | rc₂ = rc′ c₂ rc₂

  envTheorem : ∀ {Γ} → (ρ : Env Γ) → ReducibleEnv ρ

  envTheorem ε = tt
  envTheorem (ρ , c) = theorem c , envTheorem ρ


termination : {σ : Ty} → (c : Closed σ) → Trace (decompose c)

termination {○} c = theorem c
termination {σ ⇒ τ} c = proj₁ (theorem c)

evaluate : ∀ {σ} → (c : Closed σ) → (∃ λ (c′ : Closed σ) → Value c′)

evaluate c = iterate (decompose c) (termination c)
