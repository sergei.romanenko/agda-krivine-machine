# Krivine machine

Experiments with the code accompanying

* Wouter Swierstra. From mathematics to abstract machine: a formal
  derivation of an executable Krivine machine.
  In Proceedings of the 4th Workshop on Mathematically Structured
  Functional Programming, pages 163--177, 2012.
  DOI=[10.4204/EPTCS.76.10](https://doi.org/10.4204/EPTCS.76.10)
