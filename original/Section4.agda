--  ******************************************* --
--           Iterated head reduction            --
--  ******************************************* --
module Section4 where

open import Data.List
open import Data.Empty
open import Data.Unit

open import Data.Product
open import Function

open import Relation.Binary.PropositionalEquality

open ≡-Reasoning

open import Terms
open import Redex

data Trace : ∀ {u} → {c : Closed u} → Decomposition c → Set where
  Done : ∀ {u v G} → (body : Term (u ∷ G) v) → (env : Env G) →
           Trace (val body env)
  Step : ∀ {u v : Ty} {r : Redex u} {ctx : EvalContext u v} →
           Trace (decompose (plug ctx (contract r))) →
           Trace (decomposed r ctx)

iterate : ∀ { u : Ty} → {c : Closed u} → (d : Decomposition c) → Trace d →
            Value u
iterate (val body env) (Done .(body) .(env)) =
  val (closure (lam body) env) tt
iterate (decomposed r ctx) (Step step) =
  iterate (decompose (plug ctx (contract r))) step

Reducible : {u : Ty} → (t : Closed u) → Set
Reducible {○} t = Trace (decompose t)
Reducible {u ⇒ v} t = (Trace (decompose t)) ×
                      ((x : Closed u) → Reducible x → Reducible (clapp t x))

ReducibleEnv : ∀ {G} → Env G → Set
ReducibleEnv ε = ⊤
ReducibleEnv (x · env) = (Reducible x) × (ReducibleEnv env)

deref : ∀ {G u} (env : Env G) (r : Ref G u) → ReducibleEnv env →
          Reducible (env ! r)
deref ε () _
deref (x · _) top (red , _) = red
deref (_ · env) (pop i) (_ , redEnv) = deref env i redEnv

step : ∀ {u} → (c : Closed u) → (t : Trace (decompose (headReduce c))) →
         Trace (decompose c)
step c trace with decompose c
step ._ trace | val body env = Done body env
step ._ trace | decomposed r ctx = Step {r = r} {ctx = ctx} trace

lemma1 : ∀ {u} → (t : Closed u) → Reducible (headReduce t) → Reducible t
lemma1 {○} t red = step t red
lemma1 {u ⇒ v} (closure (lam body) env) (_ , red) = 
  (Done body env , \x redX → red x redX)
lemma1 {u ⇒ v} (closure (app f x) env) (wn , red) = 
  (step (closure (app f x) env) wn ,
        \y redY → lemma1 (clapp (closure (app f x) env) y) (red y redY))
lemma1 {u ⇒ v} (closure (var i) env) (wn , red) = 
  (step (closure (var i) env) wn ,
        λ x redX → lemma1 (clapp (closure (var i) env) x) (red x redX))
lemma1 {u ⇒ v} (clapp {s} f x) (wn , red) = 
  step (clapp f x) wn ,
       \y redY → lemma1 (clapp (clapp f x) y) (forceStep y (red y redY))
  where
  forceStep : (y : Closed u) →
    Reducible (clapp (headReduce (clapp f x)) y) → 
    Reducible (headReduce (clapp (clapp f x) y) )
  forceStep y h
    rewrite headReduceLemma (clapp f x) y (clapp (clapp f x) y) refl id = h

lemma2 : ∀ {G u} → (t : Term G u) (env : Env G) → 
  (ReducibleEnv env) → Reducible (closure t env)
lemma2 (lam {G} {s} {t} body) env redenv = 
  let bodyRed : (term : Closed s) → Reducible term → 
                Reducible (closure body (term · env))
      bodyRed t redT = lemma2 body (t · env) (redT , redenv)
  in (Done body env ,
      \x redX → lemma1 (clapp (closure (lam body) env) x) (bodyRed x redX))
lemma2 (app f x) env redenv =
  let redF : Reducible (closure f env)
      redF = lemma2 f env redenv
  in let redX : Reducible (closure x env)
         redX = lemma2 x env redenv
  in lemma1 (closure (app f x) env) (proj₂ redF (closure x env) redX)
lemma2 (var i) env redenv = 
  let redVar : Reducible (env ! i)
      redVar = deref env i redenv 
  in lemma1 (closure (var i) env) redVar

mutual    
  theorem : ∀ {u} → (c : Closed u) → Reducible c
  theorem (closure t env) = lemma2 t env (envTheorem env)
  theorem (clapp f x) = proj₂ (theorem f) x (theorem x)

  envTheorem : ∀ {G} → (env : Env G) → ReducibleEnv env
  envTheorem ε = tt
  envTheorem (t · ts) = (theorem t , envTheorem ts)

termination : {u : Ty} → (c : Closed u) → Trace (decompose c)
termination {○} c = theorem c
termination {u ⇒ v} c = proj₁ (theorem c)

evaluate : ∀ {u} → Closed u → Value u
evaluate t = iterate (decompose t) (termination t)

