--  ******************************************* --
--                    Refocusing                --
--  ******************************************* --
module Section5 where

open import Data.List
open import Data.Empty
open import Data.Unit

open import Data.Product
open import Function

open import Relation.Binary.PropositionalEquality

open ≡-Reasoning

open import Terms
open import Redex
import Section4

refocus : ∀ {u v} → (ctx : EvalContext u v) (c : Closed u) →
            Decomposition (plug ctx c)
refocus MT (closure (lam body) env) = val body env
refocus (ARG x ctx) (closure (lam body) env) = decomposed (beta body env x) ctx
refocus ctx (closure (var i) env) = decomposed (ref i env) ctx
refocus ctx (closure (app f x) env) = decomposed (app f x env) ctx
refocus ctx (clapp f x) = refocus (ARG x ctx) f

refocusCorrect : ∀ { v u} → (ctx : EvalContext u v) (c : Closed u) → 
   refocus ctx c ≡ decompose (plug ctx c)

refocusCorrect MT (closure (lam body) env) = refl
refocusCorrect (ARG x ctx) (closure (lam body) env)
  rewrite decomposePlug ctx (clapp (closure (lam body) env ) x) = refl
refocusCorrect MT (closure (var i) env) = refl
refocusCorrect (ARG x ctx) (closure (var i) env) 
  rewrite decomposePlug ctx (clapp (closure (var i) env) x) = refl
refocusCorrect MT (closure (app f x) env) = refl
refocusCorrect (ARG arg ctx) (closure (app f x) env) 
  rewrite decomposePlug ctx (clapp (closure (app f x) env) arg) = refl
refocusCorrect MT (clapp f x) = refocusCorrect (ARG x MT) f 
refocusCorrect (ARG arg ctx) (clapp f x) =
  refocusCorrect (ARG x (ARG arg ctx)) f

data Trace : {u : Ty} {c : Closed u} → Decomposition c → Set where
   Done : ∀ {G u v} → (body : Term (u ∷ G) v) → (env : Env G) →
            Trace (val body env)
   Step : ∀ {u v} → {r : Redex u} {ctx : EvalContext u v} →
            Trace (refocus ctx (contract r)) →
            Trace  (decomposed r ctx)

termination : ∀ {u} → (c : Closed u) → Trace (refocus MT c)

termination t rewrite refocusCorrect MT t = lemma t (Section4.termination t)
  where 
    rewriteStep : ∀ {u v} → (ctx : EvalContext u v) (t : Closed u) → 
      Trace (decompose (plug ctx t)) → Trace (refocus ctx t)
    rewriteStep ctx t p rewrite refocusCorrect ctx t = p
    lemma : ∀ {u} → (t : Closed u) → Section4.Trace (decompose t) →
              Trace (decompose t)
    lemma t p with decompose t
    lemma ._ p | val body env = Done body env
    lemma {u} .(plug ctx (fromRedex r)) (Section4.Step p) | decomposed r ctx =
      Step {r = r} {ctx = ctx} (rewriteStep ctx (contract r)
           (lemma (plug ctx (contract r)) p))

iterate : ∀ {u} → {c : Closed u} → (d : Decomposition c) → Trace d →
            Value u
iterate (val body env) (Done .(body) .(env)) =
  val (closure (lam body) env) tt
iterate (decomposed r ctx) (Step step) =
  iterate (refocus ctx (contract r)) step

evaluate : ∀ {u} → Closed u → Value u
evaluate c = iterate (refocus MT c) (termination c)

correctness : ∀ {u} → {t : Closed u}→ 
  (trace : Trace (refocus MT t)) → (trace' : Section4.Trace (decompose t)) →
  iterate (refocus MT t) trace ≡ Section4.iterate (decompose t) trace'

correctness {u} {t} t1 t2 rewrite refocusCorrect MT t = lemma t t1 t2
  where
  lemma : ∀ {u} → (t : Closed u) → (t1 : Trace (decompose t)) →
            (t2 : Section4.Trace (decompose t)) →
            iterate (decompose t) t1 ≡ Section4.iterate (decompose t) t2
  lemma t t1 t2 with decompose t
  lemma .(closure (lam body) env) (Done .body .env)
        (Section4.Done .body .env) | val body env = refl
  lemma .(plug ctx (fromRedex r)) (Step t1)
        (Section4.Step t2) | decomposed r ctx 
        rewrite refocusCorrect ctx (contract r) =
        lemma (plug ctx (contract r)) t1 t2

corollary : ∀ {u} → (t : Closed u) → evaluate t ≡ Section4.evaluate t
corollary t = correctness (termination t) (Section4.termination t)
