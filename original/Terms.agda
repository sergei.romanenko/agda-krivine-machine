--  ******************************************* --
--                 Basic Terms                  --
--  ******************************************* --
module Terms where

open import Data.List
open import Data.Empty
open import Data.Unit

data Ty : Set where
  ○ : Ty
  _⇒_ : Ty → Ty → Ty

Context : Set
Context = List Ty

data Ref : Context → Ty → Set where
  top : ∀ {G u} → Ref (u ∷ G) u
  pop : ∀ {G u v} → Ref G u → Ref (v ∷ G) u

data Term : Context → Ty → Set where
  lam : ∀ {G u v} → Term (u ∷ G) v → Term G (u ⇒ v)
  app : ∀ {G u v} → Term G (u ⇒ v) → Term G u → Term G v
  var : ∀ {G u} → Ref G u → Term G u

mutual

  data Closed : Ty → Set where
    closure : ∀ {G u} → Term G u → Env G → Closed u
    clapp   : ∀ {u v} → Closed (u ⇒ v) → Closed u → Closed v

  data Env : Context → Set where
    ε   : Env []
    _·_ : ∀ {G u} → Closed u → Env G → Env (u ∷ G)

isVal : ∀ {u} → Closed u → Set
isVal (closure (lam body) env) = ⊤
isVal _ = ⊥

data Value (u : Ty) : Set where
  val : (c : Closed u) → isVal c → Value u
