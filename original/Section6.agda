--  ******************************************* --
--            The Krivine Machine               --
--  ******************************************* --
module Section6 where

open import Data.List
open import Data.Empty
open import Data.Unit

open import Data.Product
open import Function

open import Relation.Binary.PropositionalEquality

open ≡-Reasoning

open import Terms
open import Redex
import Section5

mutual

  isValidClosure : ∀ {u} → Closed u → Set
  isValidClosure (closure t env) = isValidEnv env
  isValidClosure (clapp f x) = ⊥

  isValidEnv : ∀ {D} → Env D → Set
  isValidEnv ε = ⊤
  isValidEnv (c · env) = (isValidClosure c) × (isValidEnv env)

isValidContext : ∀ {u v} → EvalContext u v → Set
isValidContext MT = ⊤
isValidContext (ARG (closure t env) ctx) =
  (isValidEnv env) × (isValidContext ctx)
isValidContext (ARG (clapp f x) env) = ⊥

getContext : ∀ {u} → Σ (Closed u) isValidClosure → Context
getContext (closure {G} t env , _) = G
getContext (clapp f x , ())

getEnv : ∀ {u} → (c : Σ (Closed u) isValidClosure) → Env (getContext c)
getEnv (closure t env , p) = env
getEnv (clapp f x , ())

getTerm : ∀ {u} → (c : Σ (Closed u) isValidClosure) → Term (getContext c) u
getTerm (closure t env , p) = t
getTerm (clapp f x , ())

lookup : ∀ {u G} → Ref G u → (env : Env G) → isValidEnv env → 
  Σ (Closed u) isValidClosure
lookup top (closure t env · _) (p1 , p2) = closure t env , p1
lookup top (clapp _ _ · _) (() , _)
lookup (pop i) (_ · env) (_ , p) = lookup i env p

lookupClosure : ∀ {u G} → (env : Env G) (i : Ref G u) →
                  (p : isValidEnv env) →  
                  let c = lookup i env p in
                  env ! i ≡ closure (getTerm c) (getEnv c)
lookupClosure ε () p
lookupClosure (closure t env · env') top (y1 , y2) = refl
lookupClosure (clapp f x · env) top (() , y2)
lookupClosure (c · env) (pop i) (_ , p) = lookupClosure env i p

lookupLemma : ∀ {u G} → (env : Env G) (i : Ref G u) (p : isValidEnv env) →
                isValidEnv (getEnv (lookup i env p))
lookupLemma ε () p
lookupLemma (closure t env · _) top (p , _) = p
lookupLemma (clapp f x · env) top (() , p)
lookupLemma (c · env) (pop i) (p1 , p2) = lookupLemma env i p2

data Trace : ∀ {G u v} → Term G u → Env G → EvalContext u v → Set where
  Lookup : ∀ {u v G} → {ctx : EvalContext u v} {env : Env G} (i : Ref G u)
             (p : isValidEnv env) → 
             let c = lookup i env p in
             Trace (getTerm c) (getEnv c) ctx → Trace (var i) env ctx
  App : ∀ { u G v w} → {env : Env G} {ctx : EvalContext v w}
          (f : Term G (u ⇒ v)) (x : Term G u) →
          Trace f env (ARG (closure x env) ctx) →
          Trace (app f x) env ctx
  Beta : ∀ {G u v w H} → {env : Env G} (ctx : EvalContext u w) →
           (arg : Term H v) → (argEnv : Env H) →
           (body : Term (v ∷ G) u) → 
           Trace body (closure arg argEnv · env) ctx →
           Trace (lam body) env (ARG (closure arg argEnv) ctx)
  Done : ∀ {G u v} → {env : Env G} (body : Term (v ∷ G) u) →
           Trace (lam body) env MT

refocus : ∀ {G u v} → (ctx : EvalContext u v) (t : Term G u) (env : Env G) →
            Trace t env ctx → Value v
refocus ctx .(var i) env (Lookup i q step) = 
  let c = lookup i env q in
  refocus ctx (getTerm c) (getEnv c) step
refocus ctx .(app f x) env (App f x step) =
  refocus (ARG (closure x env) ctx) f env step
refocus .(ARG (closure arg env') ctx) .(lam body) env
        (Beta ctx arg env' body step) = 
  refocus ctx body ((closure arg env') · env) step
refocus .(MT) .(lam body) env (Done body) =
  val (closure (lam body) env) tt

invariant : ∀ { G u v} → EvalContext u v → Env G → Set
invariant ctx env = (isValidEnv env) × (isValidContext ctx)

termination : ∀ {u} → (t : Term [] u) → Trace t ε MT
termination t = lemma MT t ε (tt , tt) (Section5.termination (closure t ε))
  where
  lemma : ∀ {G u v} → (ctx : EvalContext u v) (t : Term G u) (env : Env G) →
            invariant ctx env →
            Section5.Trace (Section5.refocus ctx (closure t env)) → 
            Trace t env ctx    
  lemma MT (lam .body) .env p (Section5.Done body env) = Done body
  lemma MT (app f x) env p (Section5.Step step) =
    App f x (lemma (ARG (closure x env) MT) f env ((proj₁ p) , p) step)
  lemma MT (var i) env (p1 , p2) (Section5.Step step)
    rewrite lookupClosure env i p1 =
      Lookup i p1 (lemma MT (getTerm (lookup i env p1))
      (getEnv (lookup i env p1)) (lookupLemma env i p1 , p2) step)
  lemma (ARG (closure arg env') ctx) (lam body) env (p1 , (p2 , p3))
        (Section5.Step step) =
    Beta ctx arg env' body (lemma ctx body ((closure arg env') · env)
         ((p2 , p1) , p3) step)
  lemma (ARG (clapp _ _) ctx) (lam _) env (_ , ()) (Section5.Step _)
  lemma (ARG arg ctx) (app (lam body) x) env p (Section5.Step step) =
    App (lam body) x (lemma (ARG (closure x env) (ARG arg ctx))
    (lam body) env ((proj₁ p) , p) step)
  lemma (ARG arg ctx) (app (app f y) x) env p (Section5.Step step) =
    App (app f y) x (lemma (ARG (closure x env) (ARG arg ctx))
    (app f y) env ((proj₁ p) , p) step)
  lemma (ARG arg ctx) (app (var i) x) env p (Section5.Step step) 
    rewrite lookupClosure env i (proj₁ p) =
      let c = lookup i env (proj₁ p) in
      App (var i) x (lemma (ARG (closure x env) (ARG arg ctx))
      (var i) env ((proj₁ p) , p) step)
  lemma (ARG arg ctx) (var i) env p (Section5.Step step) 
    rewrite lookupClosure env i (proj₁ p) =
      let c = lookup i env (proj₁ p) in
      Lookup i (proj₁ p) (lemma (ARG arg ctx) (getTerm c) (getEnv c)
      ((lookupLemma env i (proj₁ p)) , (proj₂ p)) step)

evaluate : ∀ {u} → Term [] u → Value u
evaluate t = refocus MT t ε (termination t)

correctness :
  ∀ {u v G} →
    (ctx : EvalContext u v) (t : Term G u) (env : Env G) →
    (t1 : Trace t env ctx) → 
    (t2 : Section5.Trace (Section5.refocus ctx (closure t env))) →
    refocus ctx t env t1 ≡
      Section5.iterate (Section5.refocus ctx (closure t env)) t2

correctness MT .(var i) env (Lookup i p t1) (Section5.Step t2) 
  rewrite lookupClosure env i p =
    let c = lookup i env p in
    correctness MT (getTerm c) (getEnv c) t1 t2
correctness MT .(app f x) env (App f x t1) (Section5.Step t2) =
  correctness (ARG (closure x env) MT) f env t1 t2
correctness MT .(lam body) .env (Done .body) (Section5.Done body env) = refl
correctness (ARG arg ctx) .(var i)  env (Lookup i p t1) (Section5.Step t2) 
  rewrite lookupClosure env i p =
    let c = lookup i env p in
    correctness (ARG arg ctx) (getTerm c) (getEnv c) t1 t2
correctness (ARG arg ctx) .(app f x) env (App f x t1) (Section5.Step t2) =
  correctness (ARG (closure x env) (ARG arg ctx)) f env t1 t2
correctness (ARG .(closure arg argEnv) ctx) .(lam body) env
            (Beta ._ arg argEnv body t1) (Section5.Step t2) =
  correctness ctx body (closure arg argEnv · env) t1 t2

corollary : ∀ {u} → (t : Term [] u) →
              evaluate t ≡ Section5.evaluate (closure t ε)
corollary t =
  let trace   = termination t in
  let trace'  = Section5.termination (closure t ε) in
  correctness MT t ε trace trace'


