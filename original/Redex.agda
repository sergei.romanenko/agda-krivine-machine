--  ******************************************* --
--            Redex and contraction             --
--  ******************************************* --
module Redex where

open import Data.List
open import Data.Empty
open import Data.Unit

open import Relation.Binary.PropositionalEquality

open ≡-Reasoning

open import Terms  

data Redex : Ty → Set where
  ref  : ∀ {G u} → Ref G u → Env G → Redex u
  app  : ∀ {G u v} → Term G (u ⇒ v) → Term G u → Env G → Redex v
  beta : ∀ {G u v} → Term (u ∷ G) v → Env G → Closed u → Redex v

fromRedex : ∀ {u} → Redex u → Closed u
fromRedex (ref i env) = closure (var i) env
fromRedex (app f x env) = closure (app f x) env
fromRedex (beta body env arg) = clapp (closure (lam body) env) arg

_!_ : ∀ {G u} → Env G → Ref G u → Closed u
ε        ! ()
(x · _)  ! top = x  
(x · xs) ! pop r = xs ! r

contract : ∀ {u} → Redex u → Closed u
contract (ref i env) = env ! i
contract (app f x env) = clapp (closure f env) (closure x env)
contract (beta body env arg) = closure body (arg · env)

data EvalContext : Ty → Ty → Set where
  MT : ∀ {u} → EvalContext u u
  ARG : ∀ {u v w} → Closed u → EvalContext v w → EvalContext (u ⇒ v) w

plug : ∀ {u v} → EvalContext u v → Closed u → Closed v
plug MT f = f
plug (ARG x ctx) f = plug ctx (clapp f x)

data Decomposition : ∀ {u} → Closed u → Set where
   val : ∀ {u v G} → (body : Term (u ∷ G) v) → (env : Env G) → 
     Decomposition (closure (lam body) env)
   decomposed : ∀ {v u} → (r : Redex u) → (ctx : EvalContext u v) → 
     Decomposition (plug ctx (fromRedex r))

mutual

  load : ∀ {u v} → (ctx : EvalContext u v) (c : Closed u) →
           Decomposition (plug ctx c)
  load ctx (closure (lam body) env) = unload ctx body env
  load ctx (closure (app f x) env) = decomposed (app f x env) ctx
  load ctx (closure (var i) env) = decomposed (ref i env) ctx
  load ctx (clapp f x) = load (ARG x ctx) f

  unload : ∀ {u v w G} →
    (ctx : EvalContext (u ⇒ v) w) (body : Term (u ∷ G) v) (env : Env G) →
    Decomposition (plug ctx (closure (lam body) env))
  unload MT body env = val body env
  unload (ARG arg ctx) body env = decomposed (beta body env arg) ctx

decompose : ∀ {u} → (c : Closed u) → Decomposition c
decompose c = load MT c

headReduce : ∀ {u} → Closed u → Closed u
headReduce c with decompose c
headReduce .(closure (lam body) env) | val body env =
  closure (lam body) env
headReduce .(plug ctx (fromRedex redex)) | decomposed redex ctx =
  plug ctx (contract redex)

--  ******************************************* --
--          Lemmas about decomposition          --
--  ******************************************* --

decomposePlug : ∀ {u v} → (ctx : EvalContext u v) (c : Closed u) →
  decompose (plug ctx c) ≡ load ctx c
decomposePlug MT c = refl
decomposePlug (ARG x ctx) t 
  rewrite decomposePlug ctx (clapp t x) = refl

decomposeRedex : ∀ {u v} → (ctx : EvalContext u v) (r : Redex u) →
  load ctx (fromRedex r) ≡ decomposed r ctx
decomposeRedex MT (ref i env) = refl
decomposeRedex (ARG arg ctx) (ref i env) = refl
decomposeRedex MT (app f x env) = refl
decomposeRedex (ARG arg ctx) (app f x env) = refl
decomposeRedex MT (beta body env x) = refl
decomposeRedex (ARG arg ctx) (beta body env x) = refl

headReducePlug : ∀ {u v} → (ctx : EvalContext u v) (r : Redex u) →
  headReduce (plug ctx (fromRedex r)) ≡ plug ctx (contract r)
headReducePlug ctx r 
  rewrite decomposePlug ctx (fromRedex r)
    | decomposeRedex ctx r = refl


--  ******************************************* --
--                The Snoc View                 --
--  ******************************************* --

snoc : ∀ {u v w} → EvalContext u (v ⇒ w) → Closed v → EvalContext u w
snoc MT u = ARG u MT
snoc (ARG arg ctx) u = ARG arg (snoc ctx u)

data SnocView : {u v : Ty} → EvalContext u v → Set where
  Nil  : ∀ {u} → SnocView {u} {u} MT
  Snoc : ∀ {u v w} → (x : Closed v) (ctx : EvalContext u (v ⇒ w)) →
           SnocView (snoc ctx x)

viewSnoc : ∀ {u v} → (ctx : EvalContext u v) → SnocView ctx
viewSnoc MT = Nil
viewSnoc (ARG arg ctx) with viewSnoc ctx
viewSnoc (ARG arg .MT) | Nil = Snoc arg MT
viewSnoc (ARG arg .(snoc ctx x)) | Snoc x ctx = Snoc x (ARG arg ctx)

--  The snocClapp function is the main property of snoc that we use
snocClapp : ∀ {u v w} →
  (ctx : EvalContext u (v ⇒ w)) (x : Closed v) → 
  (t : Closed u) →
  plug (snoc ctx x) t ≡ clapp (plug ctx t) x
snocClapp MT x t = refl
snocClapp (ARG arg ctx) x t rewrite snocClapp ctx x (clapp t arg) = refl

--  Some useful auxiliary properties of equalities
clappL : ∀ {u v} →
  {f f' : Closed (u ⇒ v)} {x x' : Closed u} →
  clapp f x ≡ clapp f' x' → f ≡ f'
clappL refl = refl

clappR : ∀ {u v} →
  {f f' : Closed (u ⇒ v)} {x x' : Closed u} →
  clapp f x ≡ clapp f' x' → x ≡ x'
clappR refl = refl

clappU : ∀ {u u' v} →
  {f : Closed (u ⇒ v)} {f' : Closed (u' ⇒ v)}
  {x : Closed u} {x' : Closed u'} →
  clapp f x ≡ clapp f' x' → u ≡ u'
clappU refl = refl

--  ******************************************* --
--     The property needed in lemma1            --
--  ******************************************* --

headReduceLemma : ∀ {u v} →
  (f : Closed (u ⇒ v)) (x : Closed u)
  (fx : Closed v) →
  (clapp f x ≡ fx) →
  (isVal f → ⊥) →
  headReduce fx ≡ clapp (headReduce f) x
headReduceLemma f x fx eq p with decompose fx
headReduceLemma f x ._ () p | val body env
headReduceLemma f x .(plug ctx (fromRedex r)) eq p | decomposed r ctx
  with viewSnoc ctx
headReduceLemma f x ._ () p | decomposed (ref _ _) .MT | Nil
headReduceLemma f x ._ () p | decomposed (app _ _ _) .MT | Nil
headReduceLemma f x ._ eq p | decomposed (beta _ _ _) .MT | Nil with clappU eq
... | refl rewrite (clappL eq) = ⊥-elim (p tt)
headReduceLemma f x' ._ eq p | decomposed r .(snoc ctx x) | Snoc x ctx
  rewrite snocClapp ctx x (contract r) | snocClapp ctx x (fromRedex r)
  with clappU eq
... | refl rewrite clappR eq | clappL eq | headReducePlug ctx r = refl

